+++
title = "GUI"
date = 2020-08-05T12:10:53-04:00
weight = 4
chapter = true
pre = "<b>4. </b>"
+++

### Chapter 4

# The Doxa Graphical User Interface

This chapter explains how to use the Doxa graphical user interface (GUI).  The GUI main menu is divided into the following sections:

- App
- Prepare
- Generate
- Publish
- View
- Run
- User

{{% notice tip %}}Think of creating a liturgical website as three major steps: Prepare, Generate, and Publish. Both after generating and publishing you will want to view the website. 
{{% /notice %}}