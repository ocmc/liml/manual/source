+++
title = "Stylesheet"
date = 2020-08-05T12:10:53-04:00
weight = 1
chapter = false
pre = "<b>4.2.4 </b>"
+++

When you click on the ```Prepare > Stylesheet``` menu option, your liturgical website stylesheet file (```app.css```) will open in an editor.  Use the editor to make modifications. You must manually save the file after making changes.

{{% notice info %}}In your ```doxa/sites``` directory, you can have one or more named sites.  For each named site, there is an ```app.css``` file in the ```doxa/sites/{sitename}/assets/css``` directory.
{{% /notice %}}

A Cascading StyleSheet (CSS) file is used by a web browser to change the appearance of elements on a webpage, e.g. font size, color, weight, etc.  

{{% notice warning %}}Do not edit the stylesheet file unless you understand what a cascading stylesheet is and how to create rules for it! 
{{% /notice %}}

In Doxa, there is a direct relationship between the statements in a template and the format of liturgical text.

For example, consider this statement from a template:

```
p.actor sid "actors/Priest"
```

Doxa will look up ```actors/Priest``` in the database library you specify (e.g. en_uk_lash) and generate this HTML:

```
<p class="actor">PRIEST</p>
```

In order for this to work, there must be a rule for ```p.actor``` in the ```app.css``` file, e.g.:

```
p.actor {
    text-align:left;
    font-size:80%;
    color:red;
    font-weight:bold;
    text-indent: 0px;
    margin-top: 5px;
    margin-bottom: 0px;
}
```

{{% notice warning %}}Changes you make to the ```app.css``` file can affect the autocompletion in the template editor. The autocomplete popup reads the ```app.css``` file and extracts all rules for paragraph classes (e.g. ```p.actor```), span classes (e.g. ```span.italics```), and line break classes (e.g. ```br.single```).  If you add a new class to your css file, you must prefix it with the appropriate html tag (one of these three:  br, p, or span). Do not use a classname without a tag, e.g. do not do this: ```.actor { ... }```
{{% /notice %}}

{{% notice tip %}}You can also edit the ```css.js``` file using any text editor that is installed on your computer.
{{% /notice %}}
