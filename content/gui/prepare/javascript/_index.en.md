+++
title = "Javascript"
date = 2020-08-05T12:10:53-04:00
weight = 1
chapter = false
pre = "<b>4.2.1 </b>"
+++

When you click on the ```Prepare > Javascript``` menu option, your liturgical website javascript file (```app.js```) will open in an editor.  Use the editor to make modifications. You must manually save the file after making changes.

{{% notice info %}}In your ```doxa/sites``` directory, you can have one or more named sites.  For each named site, there is an ```app.js``` file in the ```doxa/sites/{sitename}/assets/js``` directory.
{{% /notice %}}

Javascript is a programming language that is executed by a web browser.  It provides actions to a website, e.g. something happens when you click a button.  Most people will not need to modify the javascript for the their generated liturgical website.  But, if you need to, you can.

{{% notice warning %}}Do not edit the javascript file unless you know what you are doing! 
{{% /notice %}}

{{% notice tip %}}You can also edit the ```app.js``` file using any text editor that is installed on your computer.
{{% /notice %}}
