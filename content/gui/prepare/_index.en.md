+++
title = "Prepare"
date = 2020-08-05T12:10:53-04:00
weight = 2
chapter = false
pre = "<b>4.2 </b>"
+++

# Preparing to Generate a Liturgical Website

This section explains the tasks you need to do to prepare for generating a liturgical website.  There are five types of things that need to be prepared:

- Javascript (used to add actions to your website)
- Media (used to define musical scores and audio files for each hymn)
- Settings (used to modify the Doxa configuration)
- Templates (used to create reusable specifications of the content and order of books and services)
- Translations and Notes (used to enter translations using existing templates, and to create notes about liturgical texts)
