+++
title = "Templates"
date = 2020-08-05T12:10:53-04:00
weight = 4
chapter = false
pre = "<b>4.2.4 </b>"
+++

This section explains what templates are, how they are used by Doxa, and how to create or modify them.

When you click on the ```Prepare > Javascript``` menu option, your liturgical website javascript file (```app.js```) will open in an editor.  Use the editor to make modifications. You must manually save the file after making changes.

## What is a Template?

In Doxa, a template is a file, written in a language called ```LML```, that indicates the content that is to appear in a liturgical book or service, the order in which it appears, and how it is to be formatted.  A template does not indicate which languages, translations, or versions are to be used.  This means a template written for a service can be reused to produce the service in any language, as long as the translations in that language are available in the Doxa database. If you have ever prepared a book for publication, you know that a lot of effort is required to create the layout and formatting of the book.  With Doxa templates, that effort results in templates that can be used over and over with different languages, thus saving time and cost.

Also, the same template can be used to produce a liturgical book or service in one language, two languages, or three languages, side-by-side.

There are two main types of templates: top-level and blocks.  A top-level template is the starting point for a liturgical book or service.  A top-level template typically inserts a number of block templates.  A block template is a reusable component.  For example, the ```Our Father``` is a prayer that occurs in multiple liturgical services, sacraments, and books.  Rather than repetitively re-writing the statements to produce this prayer, it can be placed in its own block template and then inserted where needed. Because a template is a reusable component, it saves time by avoiding repetitious effort.

{{% notice info %}}In your ```doxa/sites``` directory, you can have one or more named sites.  For each named site, there is an ```templates``` folder with template files.  You know a file is a template file because it ends in ```.lml```.
{{% /notice %}}

{{% notice info %}}```LML``` stands for ```Liturgical Markup Language```. You can think of it as a liturgical programming language and statements written in LML as code. 
{{% /notice %}}

## How Does Doxa Use Templates?

## How to Create or Update a Template

When you click on the ```Prepare > Templates``` menu option, Doxa will open the template editor.  You must then pick the template you wish to edit, or create a new one. Use the editor to make modifications. You must manually save the file after making changes.

### Autocompletion

When you start typing in the template editor, a popup will automatically appear.  It will list short texts that match what you are typing.  Select the one you want and press the ```<ENTER>```, or ```<Return>``` key.  The editor will automatically insert the text.

There are four kinds of autocompletion texts: LML keywords, CSS classnames, Database Topic/Keys, and Template IDs.

- LML Keywords are the words used for writing LML code.  They include ```if```, ```switch```, etc.
- CSS Classnames start with one of four HTML tags: ```br.```, ``hr.```, ```p.```, or ```span.```.  They are followed by a CSS classname.  For example, if you type ```p.``` you will see a list of all css rules in your ```app.css``` file that are for the ```p``` tag, e.g. ```p.actor```.
- Database Topic/Keys are extracted from the ```gr_gr_cog``` library in the database.  An example is ```"actors/Priest```.  There are two types of topic/keys: specific ones and relative ones.  A specific topic/key is preceeded by the LML keyword ```sid```, which stands for ```specific ID```.  A relative topic/key is one whose value is modified (computed) relative to the liturgical properties of the date of a service.    A relative topic/key is preceeded by the LML keyword ```rid```, which stands for ```relative ID```.  When you type the word ```sid``` or ```rid```, the autocompletion popup will list all the available topic/keys for that type of ID.
- Template IDs are listed when you type the word ```insert```. You will see a list of all available templates that you can insert into the current template. There are two cautions about inserts.  First, normally, you should only insert block templates. Second, never insert a template into itself.  Doxa will report an error if you do.

To summarize, typing ```br.```, ```hr.```, ```p.```, or ```span.``` will trigger the css classname autocompletion list.  Typing ```sid``` or ```rid``` will trigger the topic/key autocompletion list.  Typing ```insert``` will trigger the template autocompletion list.  LML keywords appear as you type.



