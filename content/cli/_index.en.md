+++
title = "CLI"
date = 2020-08-05T12:10:53-04:00
weight = 3
chapter = true
pre = "<b>3. </b>"
+++

### Chapter 3

# The Doxa Command Line Interface (CLI)

This chapter explains how to use Doxa from a command (aka terminal) window. 