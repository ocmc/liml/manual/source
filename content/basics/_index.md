+++
title = "First Steps"
date = 2020-08-05T10:25:29-04:00
weight = 1
chapter = true
pre = "<b>1. </b>"
+++

### Chapter 1

# How to Start

The sections in this chapter explain how to get, install, and run Doxa on your computer.  

{{% notice tip %}}Click on the blue arrowheads to view chapter sections, or click on menu items on the left. 
{{% /notice %}}
