+++
title = "Requirements"
date =  2020-03-06T16:15:05-05:00
weight = 2
pre = ""
+++

There is nothing you have to install besides Doxa. 

Doxa runs on Linux, Macs, and Windows.

The next section explains how to download and install Doxa.