+++
title = "Gitlab"
date =  2020-03-06T16:15:05-05:00
weight = 1
+++

In order to use Doxa, you need a [Gitlab](https://gitlab.com) account, which is free.

Your gitlab account is used in Doxa for three purposes:

1. To backup your work to keep it safe.
2. To publish your liturgical website to the Internet.
3. To log into Doxa.

The gitlab website is [here](https://gitlab.com).

If you do not have a Gitlab account, you can sign up for one at [here](https://gitlab.com/users/sign_up).

Once you have a Gitlab account, you need to create a project for the website you will publish using Doxa. Below are the steps to do this.

1. After signing on to gitlab with your account, notice the blue menu bar at the top of your browser. There is a dropdown for projects, and one for groups.
2. It is best if you create a group before you create a project:

   1. In the blue menu bar, click on Groups, then "Your groups".
   2. In the upper right of the web page, there is a green button that says "New Group".  Click on it.
   3. Enter the name for your group. The group name must be a single word or acronym without any spaces.  It can be the acronym for your archdiocese, diocese, metropolis, or your parish. If so, you should have the blessing of your bishop or priest to create this group.  Otherwise, for the group name use something else.
   4. Optionally provide a description of the group.
   4. Change the visibility to Public.
   5. Click on the green button that says "Create group".
   
4. On your group page, click on the green button that says "New project".
    1. For the name of the project, use the word ```site```
    2. Optionally enter a description.
    3. Set the Visibility Level to public.
    4. Check the box that says "Initialize repository with a README".
    5. Click on the green button that says "Create project"

5. Copy the information for your web site.
    1. On the upper right side of your gitlab project, click the box that says "Clone".
    2. Where it says "Clone with HTTPS", copy the url (it starts with https://) and save it someplace. You will need it when you initialize your project in doxago.
