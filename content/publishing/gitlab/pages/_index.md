+++
title = "Pages"
date =  2020-03-06T16:15:05-05:00
weight = 2
pre = ""
+++

While logged into Gitlab, and with your project open, look on the left side for a menu with icons.  Find the one that looks like a gear.  It will likely be the last icon.  

1. Click the gear icon.
2. Click on "Pages".
3. Click on "Save".

You will "Your pages are served under" and a url.  Note that it can take up to 30 minutes for you site to appear at that url.

Write down the url.  You will need it in order to view your liturgical website on the Internet.  Also, it is the web page address you will share with others for them to view the website.

Note that if you own a domain, you can use it as the address for your liturgical website.  This is explained in another section.

