+++
title = "Gitlab Pages"
date =  2020-03-06T16:15:05-05:00
weight = 2
pre = "6.1 "
+++

Note that Gitlab allows you to publish unlimited sites, but the total size of your sites may not exceed 100mb. If your website size exceeds 100mb or will likely do so over time, you should use Netlify instead.  For this reason, it is recommended to use Netlify instead of Gitlab Pages.

However, if you want to use Gitlab Pages, this section explains the steps you need to take.

