+++
title = "Using Your Own Domain"
date =  2020-03-06T16:15:05-05:00
weight = 5
pre = ""
+++

If you own a domain, you can use it as the address for your liturgical website.

Here are the steps to take.

1. Login to Gitlab.
2. Open the project used for your liturgical website.
3. Find the gear icon (settings) on the left side of the page and click it.
4. Select "Pages".
5. Click "New Domain".
6. Enter the domain or subdomain.  If your domain already has content and you are adding the liturgical website to it, use a subdomain.
7. Click on "Create New Domain".

Gitlab will then show you a verification code, e.g.:

```_gitlab-pages-verification-code.chapel.liml.org TXT gitlab-pages-verification-code=f1d6f29318d7065d7b26df154472c819```

In the above example, it is a verification for chapel.liml.org, a (fictitious) subdomain for liml.org.

Next, log onto the website you use to manage the DNS settings for your domain.

Create a text record.  The host name is the part of the verification code before TXT.  The text value is the part after TXT.  In other words, don't include the TXT itself.