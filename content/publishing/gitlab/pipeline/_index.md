+++
title = "Pipeline"
date =  2020-03-06T16:15:05-05:00
weight = 1
pre = ""
+++

In order to publish your liturgical website, you need to set up a CI/CD pipeline. 

CI means "continuous integration" and CD means "continuous deployment".  You can think of a pipeline as a job that Gitlab runs on your behalf.

While logged into gitlab, and with your new project open:

1. Click on the button "Set up CI/CD".
2. Click "Apply a template".
3. In the filter, type html.
4. Click on HTML.
5. Click on "Commit changes".

Now that you have enabled the pipeline, each time you publish your website:

```doxago publish```

Gitlab will run a pipeline to update and publish your website.

In order to check the status of a pipeline, while logged into gitlab, and with your new project open:

1. In the left sidebar menu, click on CI/CD, then Pipelines.
2. You should see a Pipeline with a green "latest" label.
3. When the status changes to "passed", your web site has been updated.

After the status changes to "passed", here is how to find the URL for your website:

1. In the left sidebar menu, click on Settings, then Pages.
2. You will see "Your pages are served under:" and the url for your website.
3. Click on the url to view your published website.
4. Also, copy and save the url somewhere so you can view your site later.
5. It is possible to use a custom domain for your site.  That is another topic in this manual.
