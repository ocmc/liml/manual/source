+++
title = "Publishing Your Site"
date =  2020-03-06T16:15:05-05:00
weight = 4
pre = ""
+++

After you have built your liturgical site using Doxa, you can publish it to your github project, which will automatically cause it to appear on the Internet at the url you saw when you set up Gitlab Pages for your project.

{{% notice info %}}
The first production release of Doxa will have a publish command that will add, commit, and push changes. In the meantime, you can use git to do this manually.
{{% /notice %}}

Open a terminal in the folder for your realm:

```doxa/sites/{your realm}```

Then type the following commands:

```git add .```

```git commit -m "{commit message}```

```git push```
