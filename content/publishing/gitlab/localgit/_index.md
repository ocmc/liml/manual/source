+++
title = "Local Setup"
date =  2020-03-06T16:15:05-05:00
weight = 3
pre = ""
+++

{{% notice info %}}
The first production release of Doxa will automatically configure the user's local git repository.  In the meantime, the instructions below tell how to do it manually.  The instructions assume you have git installed and you know how to use it.
{{% /notice %}}

Now that you have set up your project in Gitlab, you need to set up a local git repository and set the remote to the url for your project.

In the doxa.yaml configuration file, you tell doxa which build.yaml file to use to build (generate) your liturgical site.  In it, you specify a realm and subrealm. Normally, the subrealm name should be "public".  If it is not, a public folder will automatically be created in the subrealm folder.

After you have generated your liturgical site using Doxa, the directory structure will be something like this:

```doxa/sites/{your realm}{public}```

Open a terminal in the realm folder:

```doxa/sites/{your realm}```

Issue the following git commands:

```git init```

```git remote add origin {remote url}```

```git add .```

```git commit -m "{commit message}```

```git push -u origin master```

