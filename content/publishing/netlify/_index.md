+++
title = "Netlify"
date =  2020-03-06T16:15:05-05:00
weight = 2
pre = "6.2 "
+++

This section explains the steps you need to take to publish your web site to [Netlify](https://netlify.com).

Note that a free account with Netlify allows you to publish unlimited sites, but the total size  of all your sites combined may not exceed 100gb.  Also, no one file can be larger than 1gb.