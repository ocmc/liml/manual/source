+++
title = "Publishing"
date = 2020-08-05T12:10:53-04:00
weight = 6
chapter = true
pre = "<b>6. </b>"
+++

### Chapter 6

### Publishing Your Liturgical Website to the Internet

If you have your own domain and web server, you can copy the contents of your generated website to the web server.

If you do not have your own web server, there are a number companies and organizations that provide free web site hosting.  

This chapter explains how to do this with two of them:

- Gitlab Pages (max site size 100 MB, good for small sites)
- Netlify (max site size 100 GB, good for larger sites)

For perspective: the size of the AGES Initiatives website for 2021 is 1.92 GB. So, it would not fit in a Gitlab Pages site, but would in Netlify.

Continue to the next section. 

