---
title: "Doxa Manual"
---

### Doxa! The Manual

{{% notice tip %}}Both Doxa and this user's manual are a work in progress. Doxa is still under development and not ready for use by the public. 
{{% /notice %}}


# Introduction

Doxa is a software program created by the [Orthodox Christian Mission Center](https://ocmc.org) (OCMC) as a service to the pan-Orthodox community.

This website provides a manual that explains how you can use Doxa to create Eastern Orthodox Christian liturgical books and services and publish them to the Internet.

As an example of what you can create using Doxa, visit [The Liturgy in My Language](https://liml.org).

{{% notice tip %}}The contents of this manual are organized in a _page tree structure_. Click on the contents in the menu on the left to open or close the chapters and sections of the manual. 
{{% /notice %}}

## Main features of Doxa

* **Translate once, reuse where needed**
* **Generate PDFs and HTML files from templates**
* **Generate a website of liturgical books and services**
* **Publish the liturgical website to the Internet**

