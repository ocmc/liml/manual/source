+++
title = "Conventions"
date = 2020-08-05T12:10:53-04:00
weight = 5
pre = "<b>5.1 </b>"
+++

Doxa template names follow conventions established by Fr. Seraphim Dedes. 

## Underscores

Template names can include a single or double underscore. A single underscore reminds the template builder that they can optionally make a choice about how to qualify the insert path.  A double underscore reminds the template builder that they must make a choice about how to qualify the insert path.

Consider two template blocks in the path ```Blocks/AP/me1_```...

```
Blocks
- AP
  - me1_
    - bl.*.lml
    - bl.hymn     
```

The fact that the folder ```me1_``` ends with a single underscore reminds the template builder that he/she can choose to insert all the blocks in the directory or a specific one.  To insert all blocks, the convention is to use ```bl.*```:

```
insert "Blocks/AP/me1_/bl.*"
```

The contents of this block (bl.*) are:

```
p.title span.designation sid "titles/Apolytikion"
insert "Blocks/AP/me1_/bl.hymn"
```

Note that ```bl.*``` inserts ```bl.hymn```.  The template builder can choose to insert just bl.hymn:

```
insert "Blocks/AP/me1_/bl.hymn"
```