+++
title = "Templates"
date = 2020-08-05T12:10:53-04:00
weight = 5
chapter = true
pre = "<b>5. </b>"
+++

### Chapter 5

# Templates

This chapter explains what Doxa templates are and how to create or modify them.