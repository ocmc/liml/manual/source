+++
title = "Configuration"
date = 2020-08-05T12:10:53-04:00
weight = 2
chapter = true
pre = "<b>2. </b>"
+++

### Chapter 2

# Configuration Files

Doxa has a main configuration file that you can use to change settings. You can also create a "build" configuration file for each liturgical website you want to create and publish.  This chapter explains the settings in each type of configuration file. 